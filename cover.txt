

Code Coverage Report:       
  2022-02-12 02:27:51       
                            
 Summary:                   
  Classes: 100.00% (9/9)    
  Methods: 100.00% (22/22)  
  Lines:   100.00% (113/113)

App\Controller\IndexController
  Methods: 100.00% ( 3/ 3)   Lines: 100.00% (  4/  4)
App\Controller\LoginController
  Methods: 100.00% ( 1/ 1)   Lines: 100.00% (  5/  5)
App\Controller\RegistrationController
  Methods: 100.00% ( 3/ 3)   Lines: 100.00% ( 18/ 18)
App\EventSubscribers\LocaleSubscriber
  Methods: 100.00% ( 1/ 1)   Lines: 100.00% (  5/  5)
App\Form\RegistrationFormType
  Methods: 100.00% ( 3/ 3)   Lines: 100.00% ( 31/ 31)
App\Repository\LanguageRepository
  Methods: 100.00% ( 1/ 1)   Lines: 100.00% (  2/  2)
App\Repository\UserRepository
  Methods: 100.00% ( 2/ 2)   Lines: 100.00% (  3/  3)
App\Service\Orm\RepositoryManager
  Methods: 100.00% ( 3/ 3)   Lines: 100.00% (  3/  3)
App\Service\Users\Registration
  Methods: 100.00% ( 5/ 5)   Lines: 100.00% ( 42/ 42)
