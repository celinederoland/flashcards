FROM celinederoland/php8.1-composer:latest AS composer
ENV APP_ENV=prod
ENV SYMFONY_ENV=prod
COPY ./src /app/src
COPY ./config /app/config
COPY ./bin /app/bin
COPY ./.env /app/.env
COPY ./public/index.php /app/public/index.php
COPY composer.json /app/composer.json
COPY composer.lock /app/composer.lock
COPY symfony.lock /app/symfony.lock
WORKDIR /app
RUN composer install --no-dev --optimize-autoloader

FROM node:14 AS npm
WORKDIR /app
COPY ./package.json /app/package.json
COPY ./package-lock.json /app/package-lock.json
COPY ./assets /app/assets
COPY ./templates /app/templates
COPY ./src /app/src
COPY ./webpack.config.js /app/webpack.config.js
COPY ./postcss.config.js /app/postcss.config.js
RUN npm install
RUN npm run build

FROM celinederoland/php8.1-composer:latest AS composer-test
ENV APP_ENV=test
ENV SYMFONY_ENV=test
COPY ./src /app/src
COPY ./tests /app/tests
COPY ./config /app/config
COPY ./bin /app/bin
COPY ./.env /app/.env
COPY ./.env.test /app/.env.test
COPY ./public/index.php /app/public/index.php
COPY composer.json /app/composer.json
COPY composer.lock /app/composer.lock
COPY symfony.lock /app/symfony.lock
WORKDIR /app
RUN composer install --dev --optimize-autoloader

FROM celinederoland/php8.1-apache AS php-apache
ENV APP_ENV=prod
ENV SYMFONY_ENV=prod
COPY --from=composer /app/src /var/www/html/src
COPY --from=composer /app/vendor /var/www/html/vendor
COPY ./public/index.php /var/www/html/public/index.php
COPY ./config /var/www/html/config
COPY ./templates /var/www/html/templates
COPY ./translations /var/www/html/translations
COPY ./composer.json /var/www/html/composer.json
COPY --from=npm /app/public/build /var/www/html/public/build
COPY ./.env /var/www/html/.env
WORKDIR /var/www/html

FROM celinederoland/php8.1-pcov AS php-test
COPY --from=composer-test /app/src /app/src
COPY --from=composer-test /app/tests /app/tests
COPY --from=composer-test /app/bin /app/bin
COPY --from=composer-test /app/vendor /app/vendor
COPY ./migrations /app/migrations
COPY ./composer.json /app/composer.json
COPY ./config /app/config
COPY ./templates /app/templates
COPY ./translations /app/translations
COPY ./public/index.php /app/public/index.php
COPY --from=npm /app/public/build /app/public/build
COPY ./phpunit-with-coverage.xml /app/phpunit.xml
COPY ./.env /app/.env
COPY ./.env.test /app/.env.test
WORKDIR /app
