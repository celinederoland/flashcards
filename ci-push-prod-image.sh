#!/usr/bin/env sh

docker push celinederoland/dev-apache-flashcards:${CI_COMMIT_TAG}
docker push celinederoland/dev-apache-flashcards:latest