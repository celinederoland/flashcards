<?php

namespace App\Tests\Application;

class HomePageLanguageTest extends ApplicationTest
{

    public function testUseUrlParameter(): void
    {
        $this->client->request('GET', '/es', [], [], [
            'HTTP_ACCEPT_LANGUAGE' => 'en',
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-language', 'es');
    }

    public function testUseStickyLanguage()
    {
        $this->client->request('GET', '/ru', [], [], [
            'HTTP_ACCEPT_LANGUAGE' => 'fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3',
        ]);
        $session = $this->client->getRequest()->getSession();
        $this->assertEquals('ru', $session->get('_locale'));
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-language', 'ru');

        $this->client->request('GET', '/');
        $this->assertResponseRedirects('/ru');
    }

    public function testUseAcceptLanguageHeader()
    {
        $this->client->request('GET', '/', [], [], [
            'HTTP_ACCEPT_LANGUAGE' => 'fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3'
        ]);
        $this->assertResponseRedirects('/fr');
        $this->client->followRedirect();
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-language', 'fr');
    }

    public function testUseEnglishByDefault(): void
    {
        // Call Home Page, I must be redirected to /en Home Page
        $this->client->request('GET', '/', [], [], [
            'HTTP_ACCEPT_LANGUAGE' => '',
        ]);
        $this->assertResponseRedirects('/en');

        // Call /en Home Page, Content must be in english
        $this->client->followRedirect();
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-language', 'en');
    }

    public function testResetLocaleRemovesTheStickySession()
    {
        $this->client->request('GET', '/es');
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-language', 'es');
        $this->client->request('GET', '/reset_locale');
        $this->client->request('GET', '/');
        $this->assertResponseRedirects('/en');
        $this->client->followRedirect();
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-language', 'en');
    }
}