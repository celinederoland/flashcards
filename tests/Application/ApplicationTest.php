<?php

namespace App\Tests\Application;

use App\Service\Orm\RepositoryManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

abstract class ApplicationTest extends WebTestCase
{
    protected EntityManagerInterface $entityManager;
    protected RepositoryManager      $repositoryManager;
    protected KernelBrowser          $client;

    protected function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
        $this->client->disableReboot();
        $this->entityManager     = $this->getContainer()->get(EntityManagerInterface::class);
        $this->repositoryManager = $this->getContainer()->get('orm');
        $this->entityManager->beginTransaction();
    }

    protected function tearDown(): void
    {
        $this->entityManager->rollback();
        parent::tearDown();
    }
}