<?php

namespace App\Tests\Application;

use App\Tests\Mocks\MockMailer;
use Exception;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;

class RegistrationTest extends ApplicationTest
{

    public function testRegister() {

        $crawler = $this->client->request('GET', '/en/register');
        // Validate a successful response and some content
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Register');

        $formSubmit = $crawler->selectButton('Register');
        // retrieve the Form object for the form belonging to this button
        $form = $formSubmit->form();

        // set values on a form object
        $form['registration_form[email]'] = 'tester@test.com';
        $form['registration_form[agreeTerms]'] = true;
        $form['registration_form[plainPassword]'] = 'tarTEMpion123';
        $form['registration_form[motherTong]'] = 1;
        $form['registration_form[learntLanguages]'] = [false, true, true, false];

        // submit the Form object
        $this->client->submit($form);

        $this->assertResponseRedirects('/en/registered');
        $this->client->followRedirect();
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Register');
        $this->assertSelectorTextContains('p', 'Congratulations');


        // Receive the validation email
        $mailer = $this->getContainer()->get(MailerInterface::class);
        $message = $mailer->getLastMessage();
        $this->assertInstanceOf(TemplatedEmail::class, $message);
        $mailContext = $message->getContext();
        $url         = $mailContext['signedUrl'];
        $url = parse_url($url, PHP_URL_PATH) . '?' . parse_url($url, PHP_URL_QUERY);

        // Follow the validation email
        $this->client->request('GET', $url);
        $this->assertResponseRedirects('/login');
        $this->client->followRedirect();
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Login');

        // Assert that new user is correctly saved on database
        $user = $this->repositoryManager->user()->findByEmail('tester@test.com');
        $this->assertTrue($user->isVerified());
        $this->assertEquals(1, $user->getMotherTong()->getId());
        $this->assertCount(2, $user->getLearntLanguages());
        $this->assertEquals(2, $user->getLearntLanguages()[0]->getId());
        $this->assertEquals(3, $user->getLearntLanguages()[1]->getId());
    }

    public function testRegisterWithError()
    {
        $mailer = $this->getContainer()->get(MailerInterface::class);
        $this->assertInstanceOf(MockMailer::class, $mailer);
        $mailer::$exception = new Exception('impossible to send email');

        $crawler = $this->client->request('GET', '/en/register');
        // Validate a successful response and some content
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Register');

        $formSubmit = $crawler->selectButton('Register');
        // retrieve the Form object for the form belonging to this button
        $form = $formSubmit->form();

        // set values on a form object
        $form['registration_form[email]'] = 'tester@test.com';
        $form['registration_form[agreeTerms]'] = true;
        $form['registration_form[plainPassword]'] = 'tarTEMpion123';
        $form['registration_form[motherTong]'] = 1;
        $form['registration_form[learntLanguages]'] = [false, true, true, false];

        // submit the Form object
        $this->client->submit($form);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Register');
        $this->assertSelectorTextContains('.invalid-feedback', 'An error occurred');
    }

    public function testVerifyEmailWithError()
    {
        $url = 'http://localhost/en/verify/email?expires=1644473971&signature=cyZK2JOEE3O1gCrEWh2gNiRwyaRk75GO%2FtiQlmQrpg4%3D&token=Cz7b5M9hqSoAUa5jjnMC%2Fsxengkqu%2BS8JmoaViKnt24%3D';
        $this->client->request('GET', $url);
        $this->assertResponseRedirects('/en/register');
        $this->client->followRedirect();
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('.alert-danger', 'The link to verify your email is invalid');
    }
}