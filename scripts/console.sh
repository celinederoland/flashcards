#!/usr/bin/env bash

docker compose -f docker-compose.yml -f docker-compose.local.yml run --rm flashcards-symfony php bin/console --env=dev ${@}