#!/usr/bin/env bash

docker-compose -f docker-compose.yml -f docker-compose.local.yml run --rm flashcards-testunit-cov php vendor/bin/phpunit --configuration /app/phpunit.xml ${@}