#!/usr/bin/env bash

# Start database, sleep hoping it will have enough time to start ... berk
docker compose -f docker-compose.yml -f docker-compose.local.yml up -d flashcards-bdd-tests flashcards-bdd
sleep 5

# Drop and recreate the tests database
sh console.sh doctrine:database:drop --force
sh console.sh doctrine:database:create
sh console.sh doctrine:migration:migrate --no-interaction
sh console.sh make:migration
sh console.sh doctrine:migration:migrate --no-interaction
sh console.sh doctrine:schema:update
sh console.sh doctrine:fixtures:load -n