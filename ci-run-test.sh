#!/usr/bin/env sh

docker compose -p ${CI_COMMIT_SHORT_SHA} up -d flashcards-bdd-tests
sleep 5
docker compose -p ${CI_COMMIT_SHORT_SHA} run --rm flashcards-cli php bin/console --env=test doctrine:database:drop --force
docker compose -p ${CI_COMMIT_SHORT_SHA} run --rm flashcards-cli php bin/console --env=test doctrine:database:create
docker compose -p ${CI_COMMIT_SHORT_SHA} run --rm flashcards-cli php bin/console --env=test doctrine:schema:update --force
docker compose -p ${CI_COMMIT_SHORT_SHA} run --rm flashcards-cli php bin/console --env=test doctrine:fixtures:load -n
docker compose -p ${CI_COMMIT_SHORT_SHA} run --rm flashcards-cli php vendor/bin/phpunit --configuration /app/phpunit.xml
docker compose -p ${CI_COMMIT_SHORT_SHA} down flashcards-bdd-tests || true