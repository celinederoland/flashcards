#!/usr/bin/env sh

docker build --target php-apache -t celinederoland/dev-cli-flashcards:${CI_COMMIT_TAG} .
docker tag celinederoland/dev-apache-flashcards:${CI_COMMIT_TAG} celinederoland/dev-apache-flashcards:latest