<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{

    #[Route(
        '/',
        name: 'index'
    )]
    public function index(Request $request): Response
    {
        return $this->redirectToRoute('home', ['_locale' => $request->getLocale()]);
    }

    #[Route(
        '/{_locale}',
        name: 'home',
    )]
    public function home(): Response
    {
        return $this->render('index.html.twig');
    }

    #[Route(
        '/reset_locale',
        name: 'reset_locale',
    )]
    public function resetLocal(Request $request): Response
    {
        $request->getSession()->remove('_locale');
        return $this->redirectToRoute('index');
    }
}