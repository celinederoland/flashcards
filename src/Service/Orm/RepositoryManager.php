<?php /** @noinspection PhpIncompatibleReturnTypeInspection */

namespace App\Service\Orm;

use App\Entity\Language;
use App\Entity\User;
use App\Repository\LanguageRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

class RepositoryManager
{

    public function __construct(private EntityManagerInterface $entityManager) { }

    public function user(): UserRepository
    {
        return $this->entityManager->getRepository(User::class);
    }

    public function language(): LanguageRepository
    {
        return $this->entityManager->getRepository(Language::class);
    }
}