<?php

namespace App\Service\Users;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\InvalidSignatureException;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

class Registration
{

    public function __construct(
        private UserPasswordHasherInterface $userPasswordHasher,
        private EntityManagerInterface      $entityManager,
        private TranslatorInterface         $translator,
        private VerifyEmailHelperInterface  $verifyEmailHelper,
        private MailerInterface             $mailer,
    ) {
    }

    public function initializeUser(): User
    {
        $user = new User();
        $user->setIsVerified(false);
        $user->setPassword('...');
        return $user;
    }

    public function register(User $user, string $plainPassword): ?User
    {
        $this->entityManager->beginTransaction();

        try {
            $user->setPassword($this->userPasswordHasher->hashPassword($user, $plainPassword));
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            // generate a signed url and email it to the user
            $this->sendEmailConfirmation($user, (new TemplatedEmail())
                ->to($user->getEmail())
                ->subject($this->translator->trans('emails.register.subject'))
                ->htmlTemplate('registration/confirmation_email.html.twig'));
            $this->entityManager->commit();
            return $user;
        } catch (\Exception $exception) {
            $this->entityManager->rollback();
        }
        return null;
    }

    private function sendEmailConfirmation(User $user, TemplatedEmail $email): void
    {
        $signatureComponents = $this->verifyEmailHelper->generateSignature(
            'verify_email',
            $user->getId(),
            $user->getEmail(),
            [
                'id'      => $user->getId(),
                '_locale' => $user->getMotherTong()->getShort()
            ]
        );

        $context                         = $email->getContext();
        $context['signedUrl']            = $signatureComponents->getSignedUrl();
        $context['expiresAtMessageKey']  = $signatureComponents->getExpirationMessageKey();
        $context['expiresAtMessageData'] = $signatureComponents->getExpirationMessageData();

        $email->context($context);

        $this->mailer->send($email);
    }

    /**
     * @throws VerifyEmailExceptionInterface
     */
    public function handleEmailConfirmation(string $signedUrl, ?int $userId): void
    {
        if (null === $userId) {
            throw new InvalidSignatureException();
        }

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)->find($userId);

        if (null === $user) {
            throw new InvalidSignatureException();
        }

        $this->verifyEmailHelper->validateEmailConfirmation($signedUrl, $user->getId(), $user->getEmail());

        $user->setIsVerified(true);

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}