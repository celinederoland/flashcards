<?php

namespace App\DataFixtures;

use App\Entity\Language;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserAndLanguageFeatures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // Languages

        $english = new Language();
        $english->setShort('en');
        $english->setName('English');
        $manager->persist($english);

        $spanish = new Language();
        $spanish->setShort('es');
        $spanish->setName('Español');
        $manager->persist($spanish);

        $french = new Language();
        $french->setShort('fr');
        $french->setName('Français');
        $manager->persist($french);

        $russian = new Language();
        $russian->setShort('ru');
        $russian->setName('Русский');
        $manager->persist($russian);

        // Users

        $admin = new User();
        $admin->setEmail('admin@itpassion.info');
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setMotherTong($french);
        $admin->addLearntLanguage($english);
        $admin->addLearntLanguage($russian);
        $admin->setPassword('secret');
        $admin->setIsVerified(true);
        $manager->persist($admin);

        $russianLearner = new User();
        $russianLearner->setEmail('ru@itpassion.info');
        $russianLearner->setMotherTong($russian);
        $russianLearner->addLearntLanguage($french);
        $russianLearner->addLearntLanguage($english);
        $russianLearner->addLearntLanguage($spanish);
        $russianLearner->setPassword('secret');
        $russianLearner->setIsVerified(true);
        $manager->persist($russianLearner);

        $manager->flush();
    }
}
