<?php

namespace App\Form;

use App\Entity\Language;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Rollerworks\Component\PasswordStrength\Validator\Constraints as RollConstraints;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Constraints\IsTrue;

class RegistrationFormType extends AbstractType
{
    public function __construct(private EntityManagerInterface $entityManager) { }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $allLanguages = $this->entityManager->getRepository(Language::class)->findBy([], ['name' => 'asc']);
        $builder
            ->add('email', EmailType::class, [
                'label' => 'pages.register.email'
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'mapped'      => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'You should agree to our terms.',
                    ]),
                ],
                'label'       => 'pages.register.agree-term'
            ])
            ->add('plainPassword', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped'      => false,
                'label'       => 'pages.register.password',
                'attr'        => ['autocomplete' => 'new-password'],
                'constraints' => [
                    new Constraints\NotCompromisedPassword(),
                    new RollConstraints\PasswordStrength(),
                    new Constraints\NotBlank()
                ]
            ])
            ->add('motherTong', ChoiceType::class, [
                'choices'      => $allLanguages,
                'choice_value' => 'id',
                'choice_label' => 'name',
                'label'        => 'pages.register.mother-tong'
            ])
            ->add('learntLanguages', EntityType::class, [
                'class'        => Language::class,
                'expanded'     => true,
                'multiple'     => true,
                'choice_value' => 'id',
                'choice_label' => 'name',
                'choices'      => $allLanguages,
                'label'        => 'pages.register.learnt-languages'
            ])
            ->add('register', SubmitType::class, [
                'label' => 'pages.register.submit'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
